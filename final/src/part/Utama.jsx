import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';


import Beranda from './Beranda';
import Tentang from './Tentang';
import Produk from './Produk';
import Kontak from './Kontak';

const Utama = () =>  (
  <Switch>
      <Route exact path = "/" component={Beranda} />
      <Route path = "/Beranda" component={Beranda} />

      <Route path = "/Tentang" component={Tentang} />
      <Route path = "/Produk" component={Produk} />
      <Route path = "/Kontak" component={Kontak} />
      
  </Switch>
)    
  


export default Utama;