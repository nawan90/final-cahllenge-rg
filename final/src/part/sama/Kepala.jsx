import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';


class Kepala extends Component {
  constructor(props) {
    super(props);
    this.state = { 
        activeIndex: 0,
        items : [
            {
              src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAADNCAMAAAC8cX2UAAABUFBMVEX83MX///8AAADzbE784Mn/38j94cvyZkb83sj3nYPzakv94sz/3sj84crzbU/zcFL3qZD7y7P0el34sZnyZEP2lHr8dFX7xa780731iGzmyLT82MHz077QtaPCqZgYHh1FOzb5u6MpIR/YwK60nY3+9/VcTkf0eVwcFRR1Z14iIB7/wamch3qrlIaIdWo6MS5MQDr1inAuJSKRjo2Ecmf95+P2lIFyb27HXkdnWFDtmIQFFhfpfGW6WUMABxPdZ03FUDumSTZhLiWuRjMAExfJalrj0cL5wbb6zsXeXUl3PzE9Pj9QS0nbQyTDSTQ+JSF+NSnPRi1LKCLsqZ65b1/ql4zHoJWVQDJ9enqnpqXj09Dd3NynUj//u66TlZWFZ1z7189tMSfJx8eHRTXfinmupJxLVFXEhnmTYVdkYF8VBwCshn6YWE//8e3go5tudXau4iVFAAAI6ElEQVR4nO2b+1vaWBrHk5PbIQmcQAIJCYKAXBUQhXqrxUu7irZqdabF2nY7tnbG7rj7//+27wnQ2V6e/Wlabfp+HtFjEnzOl+97ObkoCAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIAiCIHcZU1Fuewq3QmuO3vYUvj9q+3Kl/JP5bSpxekXPwW1FVczbns13QjFy+fbaCgFK1XY+Z/wMpiuG3ysR0hn1FhZ61TpI7/nRFy7neoRUW75xZSaTnmD4c48J6fnybc/r22LkS2StbLjp1LtHjuYkUjH3/dXa4XXeuO2ZfUNMpU2W8obNnG4tdfH783/96qTYo2fJudHDK/W2J/ftUBqkmnNrut4VK5VrckgO+29SuqZpwz8PryKb32qbNFTLcfoicP2hs3JN/tDZ5ipjB9tb172I+m20yJoa09dfiWJFvCSlzuhxcyfDCkuMFQ70XdKKZH6bPunQmLMuhozqpVK91NxhbHU1k2GMab8RP4orF7NKykUn+wasBrtX6oedUqnwFhRnVl9y8Ucb1QjKVvKkberc61cvzkeV0eMVMLywtaVJmZ2HA1YoaP8k+eiVNXmW0HQY4q9XKqOLS/K41Nx4qe8en2wNVzPg9lOXzEauqikzpO05Z5WKuFw5Fy9F8fwDOd7MsMRwZ+P4ZFfTM0+TbRK5czJoXv76EaT1xfnl5biqiV1eypi+XSgUmr/tPo2VSTtqdiuzi7nRs+ULqOGjc5BswSdwT38LlXxn++i4AAG/ydTF2YgVNcUnvdzalbh8A35P3d7TDo7ZoHSQGZ4UmoV7jtcjfrSiHFJ7rrh+I1ZA8uU5Nx2y+4XD2xd8ZfTdD+ciK86RmYjJBkWWcwZaLzvkQ528BtkLr880Bj07A6GuPxX3HQs+m2jJllukHKtBHT8n9VKns0LA7xvx4p0DPTtzcCo92QfZUNMWonXiLS+A7K64X1nqzJZKyyvkPDwdGe0NjzTNeXK/f1MR0yC7FS3ZCnfb2RMvyGi5XqrXP5xXxMoIytvrP87O9l7diGdv+hKXHa0gD3Nb64svCHe71ISzrvX+e1ibn1cmZV18FcHcVsDIeQm61wppNpuFpQFjkvPvyuuJZNB+c6PNt6K2TFP8esPUEiDvz9Pj0+fQtA5OmXPG/a6MZYuWYzbqEevbgrrYUVMSX6LAevRoGMrOOC/Ej7xxUmpnMWqLU3UBkjt1WdH5MnzruQRrFLa6/e4/YSOD1xvRsWfIQtRkmz5pCO/e/64fQDUbloaZAcucPhis8PVaBYT3u5LZiOD1FWON+Nb6IPMWgls/PRk+3MkMVsdRXumL4rpj+WQtelfTYFXe8H/ReHBvDk4KWy9XYTi8mab2PWY0orYiDwFZy9dDNmCDhzu7Gzt6ZnAwGHLF+2Jlz9GSedKIntlgd65e+sf69oNVcHursLGbeflg99c9aF0WeO0U/Xo9F0GzYV0+Qw6fbR0MeHCXmscaGzzph0F+r6bZuQ6ZidZ6/CNq6/3yI51BcOunzcJz6ckZb139fYfN52bJXNSa10fircPHR/rmg03ppNBsPt0T+32ROU5XKC9F9J7ImHi+XtjWhpK+u3H6C1vfdxxH6iZpm9QjfadXUP1Fcvr8/v176yldc7RU2g5oq0MW/chG+BhFneuQjUY+R1XDUKmfbxDSmVMjWcM/QRbmqoSQpcVqdXEJBtU5IaIl/DNkNZdvV2c7nVn+oJL6c4gOUeR43DDicTki0R2EL/n/OvjlaVYwefFvNKDTjeaP8ngmtRMsa6WzVvC1nQHfalIv+Fy4100wSwjSNdtULScdlnXTlpj7o+hWJSlt0K6W+soui3Xj8CPNYp8Hg2rpkkfVmKcKalZn425maPoXB95VKMiO06KuFWU5TgGZ124vACVGSsoasmA4esyQZTVMBFkO/ZRBdjJucXNlL5aEYJADz9AkkC3TwKN3Xv1YtqtLtpfOJl0ra1NqpYqpmCDYtUTKsgJLT2Qt285mi6oAxxR5UeOyPdcG1dTtdoumKcRYzJJANrVT2aIVs++4cC5bBrf1omFrCdtgNZp2YoalZdXQbSpwEw3ZSOuOS2nRnbqdcFmY8kZWr6lGWssa/MB40oG/FLAfQbahZrWsINu6FpjZmKfptgGfg8tl81gPY1fwwHRKY+PiBrJZTQpLmZyVUoanSxZVuWxXS2Q9o2jf8dpGJQjhbtYWxrKhWnHnqe0485/KVmOSXvQmcrjsbAKOm8jmSSKHbptZPZGwhDuueuK2Sj/Khh9S2rJsN/hUtukxSHVv/K4wyGsJFvyPbBoeSAUrIeksecd1hyUtHE1kU1eD7ORlOw6yYVd8LJs3LX1y6LikFTUe5nEuOzkNciOZVL10IpH6AXJ7rIWGuQ3yatCwBCMGbVlPGdQyUnraECxZoExzJ2vTsG8bXQh71eCyaY13fwdk21AAjGyidrdlU9fRsuPIDWKaUwTZZrKmu4EFeevVtOS8rboSC2yo4LLNpqu1IO1oNg1SWsL14FtShSh3Y5bTDexELPASzl2v5G4SCEceDFy+GqWqa9seT0513nZVgZpFO0lNT/CsqZjxsVRNToDVStEOVM/1vCCw7WJwx1XDihsYD2FgTkfydGBO9lDL8ezkJ2+b7Ji8Lfw9zAEq08jcI5LTjhuL+JWkr+HFrDvelP4O/vqPzulIVf/aonw2iAqmP+PnwoEwMzPe5Jcnt3X9crk83lcu+7c0v2+D4hN/bvwU0uxMvsp9VsqTJy2VGZJb4E+SK61FoZG71Xn+zYDsBYErUvIlIRfe0IVNudBt0C+HkaAskJmvXZT5gVHzZJbHtNKaFXL1/FQ2180fR22N87xHepFKbsUXzNmGyjXWwe3yRLbAwxzcNvzxwJiJ1p19M98uV8P/BDEbcwvhw/Jgcn6Bp7uZJ+VwIPfy+U6kchvITa4TK7kv795PtQbjsEcQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQ5DvxXzeW/VVvsN8cAAAAAElFTkSuQmCC',
              altText: 'Slide 1',
              caption: 'Slide 1'
            },
            {
              src: 'https://www.logolynx.com/images/logolynx/59/59fc592e93aeddf3102a0e8e50d24cb4.jpeg',
              altText: 'Slide 2',
              caption: 'Slide 2'
            },
            {
              src: 'https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/f8c4ad15079837.5628cb1e4617b.jpg',
              altText: 'Slide 3',
              caption: 'Slide 3'
            }
          ]
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
      console.log('hay', this.state.activeIndex)
    const { activeIndex } = this.state;

    const slides = this.state.items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.altText}
        >
          <img src={item.src} alt={item.altText} />
          <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        <CarouselIndicators items={this.state.items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}
export default Kepala