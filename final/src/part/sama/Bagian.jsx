import React, { Component } from 'react'
import { Card, CardImg, CardBody,
  CardTitle, CardSubtitle} from 'reactstrap';

export default class Bagian extends Component {
  render() {
    return (
      <Card>


        <CardImg top width="100%" src={this.props.image} alt={this.props.alt} />
        <CardBody>

          <CardSubtitle>{this.props.kategori}</CardSubtitle>

          <CardSubtitle>{this.props.harga}</CardSubtitle>

        </CardBody>
      </Card>
    )
  }
}
