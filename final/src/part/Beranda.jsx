import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Kepala from './sama/Kepala';
import { Container, Row, Col } from 'reactstrap';
import Bagian from './sama/Bagian';
import axios from 'axios';


class Beranda extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products:[],
      bestku:[]
    }
  }
//get data with axios method get
componentDidMount(){
axios.get('http://reduxblog.herokuapp.com/api/posts?key=barangku')
.then(res=> {
    this.setState({
      products: res.data
    })
  })  
  axios.get('http://reduxblog.herokuapp.com/api/posts/?key=best')
.then(res=> {
    this.setState({
      bestku: res.data
    })
  })  
}
  render() {
    console.log('ini data', this.state.products)
    const looping = this.state.products.map((prod, index) => {
      return(
        <Col md='4' key={index}>
        <Bagian
        image={prod.title}
        kategori ={prod.categories}
        harga={prod.content}

        />
      </Col>
      )}
    );
    const best = this.state.bestku.map((best, abc) => {
      return(
        <Col md='4' key={abc}>
        <Bagian
        image={abc.title}
        kategori ={abc.categories}
        harga={abc.content}

        />
      </Col>
      )}
    );
    return (
      <div>
        <Kepala />
        <Container className="body-content">
          <h2>Products</h2>
          <Row className='d-flex'>
            {looping}
          {/* <AppCard
          title = 'test'
          /> */}
          </Row>
        <h2>Best Seller</h2>
          <Row className='d-flex'>
            {best}
          
          </Row>
        </Container>
      </div>
    )
  }
}
export default Beranda;