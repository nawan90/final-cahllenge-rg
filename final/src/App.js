import React, { Component } from 'react';
import './App.css';
import Utama from './part/Utama';

import Bawah from './part/Bawah';
import { Link } from 'react-router-dom';
import { Layout, Header, Navigation, Drawer, Content } from 'react-mdl';

class App extends Component {
  render() {
    return (
      <div className="demo-big-content">
      <Layout>
        <Header className="header-color"
        title="Toko Online" scroll>
        <Navigation>
        <Link to = "/Beranda">Beranda </Link> 
      <Link to = "/Tentang">Tentang </Link> 
      <Link to = "/Produk">Produk</Link> 
      <Link to = "/Kontak">Kontak</Link>
        </Navigation>
        </Header>
        <Drawer title="Toko Online">
                  <Navigation>
                  <Link to = "/Bernda">Beranda </Link> 
      <Link to = "/Tentang">Tentang </Link> 
      <Link to = "/Produk">Produk</Link> 
      <Link to = "/Kontak">Kontak</Link>
                  </Navigation>
              </Drawer>
<Content>
  <div className="page-content" />
  <Utama/>
</Content>
  <Bawah />
      </Layout>
      </div>
    );
  }
}

export default App;